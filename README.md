# Test Task for server, not for front-end

Необходимо разработать REST API для сайта “Доска объявлений”
На сайте пользователи выкладывают товары, которые хотели бы продать.
Основные возможности:
 авторизация
 регистрация
 получение/редактирование данных текущегопользователя
 смена пароля (при этом необходимо указать текущий пароль)
 поиск и сортировка товаров
 загружать/удалять изображение для товара
 поиск пользователей
 создание/редактирование/удаление товара авторизованным
пользователем
Ниже описана документация по API которую необходимо реализовать.
Идентификация текущего пользователя происходит посгенерированному токену,
который передается в заголовок Authorization.
Уточнение: В тестовом задании необходимо реализовать только back-end.
Ошибки валидации имеют общий вид:
422, Unprocessable Entity
Body:
[
{"field":"title","message":"Title should contain at least 3 characters"},
...
]
field - название поля к которому относится ошибка
message - сообщение об ошибке
 
Выполнено: 

1) npm i

2) config/default.js - set user and password for database (I used 'testuser'/'task')

3) gulp up

4)run index.js

Some functionality I tested on localhost:3000.

index.html - only for visualization responses from server.

To run the tests: 'npm test' or 'mocha'.

Thank you for attention.
