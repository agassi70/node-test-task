module.exports = {
  secret: 'mysecret',
  root: process.cwd(),
  database: 'database_development',
  user: 'testuser',
  password: 'task',
  defaultImg: 'example.jpeg',
  crypto: {
    hash: {
      length:     64,
      iterations: process.env.NODE_ENV == 'production' ? 12000 : 1
    }
  },
};
