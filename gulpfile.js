const gulp = require('gulp');
const eslint = require('gulp-eslint');
const sequelize = require('./models').sequelize;
const gulpSequelize = require('gulp-sequelize')(sequelize);

gulp.task('up', gulpSequelize.up);

gulp.task('down', gulpSequelize.down);

gulp.task('lint', () => {
  return gulp.src(['**/*.js','!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});