const koa = require('koa');
const app = koa();
const config = require('config');
const path = require('path');
const fs = require('fs');
const validate = require('koa-validate');

validate(app);

app.keys = [config.secret];

const handlers = fs.readdirSync(path.join(__dirname, 'middlewares')).sort();
handlers.forEach(handler => {
  require(`./middlewares/${handler}`).init(app);
});

const Router = require('koa-router');

const router = new Router({ prefix: '/api' });

router.post('/login', require('./routes/login').post);
router.post('/register', require('./routes/register').post);
router.get('/me', require('./routes/me').get);
router.put('/me', require('./routes/updateme').put);
router.get('/user', require('./routes/searchusers').get);
router.get('/user/:id', require('./routes/userbyid').get);
router.post('/item', require('./routes/createitem').post);
router.put('/item/:id', require('./routes/updateitem').put);
router.get('/item', require('./routes/searchitems').get);
router.get('/item/:id', require('./routes/itembyid').get);
router.del('/item/:id', require('./routes/deleteitem').del);
router.post('/item/:id/image', require('./routes/uploadimage').post);
router.get('/logout', require('./routes/logout').get);
router.del('/item/:id/image', require('./routes/deleteimage').del);

app.use(router.routes());

app.listen(3000);

module.exports = app;
