const serve = require('koa-static');

module.exports.init = app => app.use(serve('public'));
