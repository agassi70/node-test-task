module.exports.init = app => app.use(function*(next) {
  try {
    yield* next;
  } catch (e) {
    console.error(e.message, e.name);

    if (e.name === 'SequelizeValidationError' || e.name === 'SequelizeUniqueConstraintError') {
      const result = e.errors.map(error => {
        const errObj = {};
        errObj.field = error.path;
        errObj.message = error.message;
        return errObj;
      });
      this.body = result;
      this.status = 422;
      return;
    }

    if (e.status === 422) {
      const result = this.errors.map(error => {
        const errObj = {};
        const key = Object.keys(error)[0];
        errObj.field = key;
        errObj.message = error[key];
        return errObj;
      });
      this.body = result;
      this.status = 422;
      return;
    }

    if (e.status) {
      this.status = e.status;
      if (e.status === 413  || e.status === 400) {
        this.body = e.message;
      }
    } else {
      this.status = 500;
    }
  }
})
