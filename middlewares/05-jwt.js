const jwt = require('koa-jwt');
const config = require('config');

module.exports.init = app => {
  app.use(jwt({
    secret: config.secret }).unless({
      path: ['/api/register', '/api/login', /\/api\/user\/[?]{1}\w*/, /\/api\/item\/[?]{1}\w*/, /\/api\/item\/\d{1,4}/]
    })
  )
}
