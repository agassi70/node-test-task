module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface
      .createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        email: {
          allowNull: false,
          type: Sequelize.STRING(50)
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(30)
        },
        phone: {
          allowNull: true,
          type: Sequelize.STRING(13)
        },
        salt: {
          allowNull: false,
          type: Sequelize.STRING
        },
        passwordHash: {
          allowNull: false,
          type: Sequelize.STRING
        }
      }, {
        engine: 'InnoDB',
        charset: 'utf8'
      })
      .then(function () {
        return queryInterface.addIndex('users', ['email'], {indexName: 'users_email', indicesType: 'UNIQUE'});
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};
