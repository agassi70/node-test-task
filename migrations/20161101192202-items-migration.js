'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface
      .createTable('items', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        title: {
          allowNull: false,
          type: Sequelize.STRING(30)
        },
        price: {
          allowNull: false,
          type: Sequelize.FLOAT
        },
        image: {
          allowNull: false,
          type: Sequelize.STRING(50)
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        user_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'users',
            key: 'id'
          },
          onDelete: 'SET NULL',
          onUpdate: 'CASCADE'
        }
      }, {
        engine: 'InnoDB',
        charset: 'utf8'
      })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('items');
  }
};
