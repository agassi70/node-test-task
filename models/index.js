const fs = require('fs');
const path = require('path');
const basename = path.basename(module.filename);
const sequelize = require('../libs/sequelize');
const db = {};

fs
  .readdirSync(__dirname)
  .forEach(file => {
    if (file.slice(-3) !== '.js' || file === basename) {
      return;
    }
    const model = sequelize.import(path.join(__dirname, file));

    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;

module.exports = db;
