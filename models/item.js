const moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  const Item = sequelize.define('Item', {
    title: DataTypes.STRING,
    price: DataTypes.INTEGER,
    image: DataTypes.TEXT,
  }, {
    classMethods: {
      associate: function (models) {
        Item.belongsTo(models.User, { foreignKey: 'user_id' });
      }
    },
    instanceMethods: {
      getData: function () {
        const result = Object.assign({}, this.dataValues);
        const temp = result.createdAt;
        result.createdAt = moment(temp).unix();
        return result;
      }
    },
    timestamps: true,
    updatedAt: false,
    freezeTableName: true,
    tableName: 'items',
  });
  return Item;
}
