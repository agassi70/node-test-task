const crypto = require('crypto');
const config = require('config');

module.exports = function(sequelize, DataTypes) {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [2, 12],
          msg: 'Name should contain from 2 to 12 characters'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: { isEmail: true }
    },
    phone: DataTypes.STRING,
    password: {
      type: DataTypes.VIRTUAL,
      validate: {
        len: {
          args: [5, 15],
          msg: 'Password should contain from 5 to 15 characters'
        }
      },
      set: function(value) {
        const salt = crypto.randomBytes(config.crypto.hash.length).toString('hex');
        this.setDataValue('salt', salt);
        const secure = crypto.pbkdf2Sync(value, salt, config.crypto.hash.iterations, config.crypto.hash.length, 'sha1').toString('hex');
        console.log(secure);
        this.setDataValue('passwordHash', secure);
      },
      get: function() {
        return this.getDataValue('password');
      }
    },
    salt: {
      type: DataTypes.STRING,
    },
    passwordHash: {
      type: DataTypes.STRING,
    }
  }, {
    instanceMethods: {
      getData: function () {
        const result = Object.assign({}, this.dataValues);
        delete result.password;
        return result;
      },
      checkPassword: function(password) {
        if (!password) return false;
        if (!this.passwordHash) return false;

        return crypto.pbkdf2Sync(password, this.getDataValue('salt'),config.crypto.hash.iterations,
            config.crypto.hash.length, 'sha1').toString('hex') == this.getDataValue('passwordHash');
      }
    },
    timestamps: false,
    freezeTableName: true,
    tableName: 'users',
  }
  );

  return User;
}
