const config = require('config');
const Item = require('../models').Item;
const User = require('../models').User;

module.exports.post = function* () {
  this.checkBody('title').notEmpty().len(3).trim()
  this.checkBody('price').notEmpty().isFloat().trim();

  if (this.errors) {
    this.throw(422);
  }

  const { title, price } = this.request.body;
  const user = yield User.findById(this.state.user.id);

  const item = yield Item.create({
    title,
    price,
    image: config.defaultImg,
    user_id: user.id
  });

  const itemForClient = yield Item.findById(item.id,  { include: [ User ] });
  const temp = itemForClient.getData();
  temp.User = temp.User.getData();
  this.body = temp;
}

