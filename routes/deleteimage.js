const Item = require('../models').Item;
const fs = require('fs');
const config = require('config');

module.exports.del = function* () {
  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  const instance = yield Item.findById(this.params.id);

  if (!instance) {
    this.throw(404);
  }

  const fileName = instance.image;
  fs.unlink(`./images/${fileName}`, (err) => {
    if (err) {
      console.log(`Error during deleting file ${instance.image} - ${err.message}.`)
    }
  });

  yield instance.update({ image: config.defaultImg });
  this.status = 200;
}
