const Item = require('../models').Item;
const fs = require('fs');

module.exports.del = function* () {
  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  let item = yield Item.findById(this.params.id);

  if (!item) {
    this.throw(404);
  }

  fs.unlink(`./images/${item.image}`, (err) => {
    if (err) {
      console.log(`Error during deleting file ${item.image} - ${err.message}.`)
    }
  });

  item.destroy();
  this.status = 200;
}
