const Item = require('../models').Item;
const User = require('../models').User;

module.exports.get = function* () {
  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  let item = yield Item.findById(this.params.id, { include: [ User ] });

  if (!item) {
    this.throw(404);
  }

  const itemForClient = item.getData();
  itemForClient.User = itemForClient.User.getData();
  this.body = itemForClient;
}
