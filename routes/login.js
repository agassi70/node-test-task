const jwt = require('koa-jwt');
const config = require('config');
const User = require('../models').User;

module.exports.post = function* () {
  this.checkBody('email').notEmpty().isEmail().trim().toLowercase();
  this.checkBody('password').notEmpty().len(5, 15).trim();

  if (this.errors) {
    this.throw(422);
  }

  const { email, password } = this.request.body;
  const user = yield User.findOne({ where: { email } });

  if (!user) {
    this.throw(422, { errors: { field: 'email', message: 'Wrong email' } });
  }

  if (!user.checkPassword(password)) {
    this.throw(422, { errors: { field: 'password', message: 'Wrong password' } });
  }

  const token = jwt.sign( { id: user.id }, config.secret);
  this.status = 200;
  this.body = { token };
}