const User = require('../models').User;

module.exports.get = function* () {

// Koa-jwt middleware that validates JSON Web Tokens and sets ctx.state.user (by default) if a valid token is provided.
  const user = yield User.findById(this.state.user.id);
  this.body = user.getData();
}
