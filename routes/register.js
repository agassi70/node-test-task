const jwt = require('koa-jwt');
const config = require('config');
const User = require('../models').User;

module.exports.post = function* () {
  this.checkBody('email').notEmpty().isEmail().trim().toLowercase();
  this.checkBody('name').notEmpty().len(2, 12).trim();
  this.checkBody('phone').optional().empty().isMobilePhone().trim();
  this.checkBody('password').notEmpty().len(5, 15).trim();

  if (this.errors) {
    this.throw(422);
  }

  const options = Object.assign({}, this.request.body);
  const user = yield User.create(options);
  const token = jwt.sign( { id: user.id }, config.secret);
  this.status = 200;
  this.body = { token };
}
