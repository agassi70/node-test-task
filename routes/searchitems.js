const Item = require('../models').Item;
const User = require('../models').User;

module.exports.get = function* () {
  this.checkQuery('title').optional().empty().len(3);
  this.checkQuery('user_id').optional().empty().isInt();
  this.checkQuery('order_by').optional().empty().in(['price', 'createdAt']).default('createdAt');
  this.checkQuery('order_type').optional().empty().in(['asc', 'desc']).default('desc');

  if (this.errors) {
    this.throw(422);
  }

  const where = {};

  if (this.query.title) {
    where.title = this.query.title;
  }

  if (this.query.user_id) {
    where.user_id = this.query.user_id;
  }

  const items = yield Item.findAll({ where, order: [ [this.query.order_by, this.query.order_type] ], include: [ User ] });
  const result = items.map(item => {
    const temp = item.getData();
    if (temp.User) temp.User = temp.User.getData();
    return temp;
  });
  this.body = result;
}
