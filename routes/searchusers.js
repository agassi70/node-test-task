const User = require('../models').User;

module.exports.get = function* () {
  this.checkQuery('email').optional().empty().isEmail().toLowercase();
  this.checkQuery('name').optional().empty().len(2, 12, 'Bad name for search');

  if (this.errors) {
    this.throw(422);
  }

  const where = {};

  if (this.query.name) {
    where.name = this.query.name;
  }

  if (this.query.email) {
    where.email = this.query.email;
  }

  const users = yield User.findAll({ where });
  const result = users.map(user => user.getData())
  this.body = result;
}
