const Item = require('../models').Item;
const User = require('../models').User;

module.exports.put = function* () {
  this.checkBody('title').optional().empty().len(3).trim()
  this.checkBody('price').optional().empty().isFloat().trim();
  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  const instance = yield Item.findById(this.params.id);

  if (!instance) {
    this.throw(404);
  }

  const newFields = {};

  for (let field in this.request.body) {
    if (this.request.body[field] !== '') {
      newFields[field] = this.request.body[field];
    }
  }
  const item = yield instance.update(newFields);
  const itemForClient = yield Item.findById(item.id,  { include: [ User ] });
  const temp = itemForClient.getData();
  if (temp.User) temp.User = temp.User.getData();
  this.body = temp;
}
