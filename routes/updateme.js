const User = require('../models').User;

module.exports.put = function* () {
  const instance = yield User.findById(this.state.user.id);

  this.checkBody('email').optional().empty().isEmail().trim().toLowercase();
  this.checkBody('name').optional().empty().len(2, 12).trim();
  this.checkBody('phone').optional().empty().match(/[+]{1}\d{12}/).trim();

  if (this.request.body.new_password && this.request.body.new_password !== '') {
    this.checkBody('current_password').notEmpty('What is your current password ?');
  }

  if (this.request.body.current_password && this.request.body.current_password !== '') {
    this.checkBody('current_password').eq(instance.password, 'Wrong current password');
    this.checkBody('new_password').notEmpty().len(5, 15).trim();
  }

  if (this.errors) {
    this.throw(422);
  }

  const newFields = {};

  for (let field in this.request.body) {
    if (this.request.body[field] !== '') {
      newFields[field] = this.request.body[field];
    }
  }

  if (newFields.new_password) {
    delete newFields.current_password;
    newFields.password = newFields.new_password;
    delete newFields.new_password;
  }

  const user = yield instance.update(newFields);
  this.body = user.getData();
}
