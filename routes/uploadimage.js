const Item = require('../models').Item;
const User = require('../models').User;
const parse = require('co-busboy');
const fs = require('fs');
const path = require('path');

module.exports.post = function* (next) {
  if (!this.request.is('multipart/*')) {
    return yield next;
  }

  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  const instance = yield Item.findById(this.params.id);

  if (!instance) {
    this.throw(404);
  }

  const formats = ['jpeg', 'jpg', 'gif', 'png', 'tiff', 'eps', 'bmp'];
  const parts = parse(this, {
    autoFields: true,
    checkFile: function (fieldname, file, filename) {
      if (!filename) {
        const err = new Error('The field "file" must be filled.')
        err.status = 400;
        return err;
      }

      const extname = path.extname(filename).toLowerCase().slice(1);

      if (!formats.includes(extname)) {
        const err = new Error('Invalid format of image.')
        err.status = 400;
        return err;
      }
    }
  });

  let fileLength = 0;
  let part = yield parts;
  const fileName = `${instance.title}${path.extname(part.filename)}`;
  const fileStream = fs.createWriteStream(`./images/${fileName}`, { flags: 'w', autoClose: true });

  while (part) {
    part.on('error', err => {
      fileStream.end();
      this.throw(400, err.message);
    });
    part.on('data', (chunk) => {
      fileLength += chunk.length;
    });
    part.pipe(fileStream);
    part = yield parts;
  }

  if (fileLength > 500000 || fileLength === 0) {
    fs.unlink(`./images/${fileName}`, (err) => {
      if (err) {
        console.log(`File ${fileName} not deleted.`);
      } else {
        console.log(`File ${fileName} deleted.`);
      }
    });
    if (fileLength === 0) {
      this.throw(400, 'This file must be not empty.');
    } else {
      this.throw(413, 'This file is too big.');
    }
  } else {
    const item = yield instance.update({image: fileName});
    const itemForClient = yield Item.findById(item.id, {include: [User]});
    const temp = itemForClient.getData();
    temp.User = temp.User.getData();
    this.body = temp;
  }
}
