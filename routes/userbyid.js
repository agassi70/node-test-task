const User = require('../models').User;

module.exports.get = function* () {
  this.checkParams('id').notEmpty().trim().toInt();

  if (this.errors) {
    this.throw(422);
  }

  const user = yield User.findById(this.params.id);

  if (!user) {
    this.throw(404);
  }

  this.body = user.getData();
}
