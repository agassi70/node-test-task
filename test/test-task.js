const server = require('..');
const request = require('request-promise').defaults({
  simple: false
});
const should = require('should');
const coMocha = require('co-mocha');

const User = require('../models').User;
const Item = require('../models').Item;

function getURL(path) {
  return `http://localhost:3000/api${path}`;
}

describe('Test REST API', function() {
  const newUserData = {
    email: 'alice@test.ru',
    name: 'Alice',
    password: '22222'
  };

  const newItemData = {
    title: 'Samsung J500',
    price: '4850'
  };

  const newPhone = { phone: '+380991112233' };

  let token;
  let itemId;

  after(function* () {
    const newUser = yield User.findOne(newUserData);
    if (newUser) newUser.destroy();
    const newItem = yield Item.findOne(newItemData);
    if (newItem) newItem.destroy();
  });

  context('Register and update User', function () {

    it('create a user', function* () {
      let response = yield request({
        method: 'post',
        url: getURL('/register'),
        json: true,
        body: newUserData
      });
      token = response.token;
      response.should.have.property('token');
    });

    it('try create a user witn invalid data', function* () {
      let response = yield request({
        method: 'post',
        url: getURL('/register'),
        json: true,
        body: {
          email: 'invalid',
          name: 'Serg',
          password: 'abc'
        }
      });
      response.should.be.instanceof(Array).and.have.lengthOf(2);
      response[0].should.have.property('field').which.eql('email');
      response[0].should.have.property('message').which.eql('email is not email format.');
    });



    it('check new user', function* () {
      let response = yield request({
        method: 'get',
        url: getURL('/me'),
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
      JSON.parse(response).email.should.eql(newUserData.email);
      JSON.parse(response).name.should.eql(newUserData.name);
    });

    it('add phone to new user', function* () {
      let response = yield request({
        method: 'put',
        url: getURL('/me'),
        headers: {
          'Authorization': `Bearer ${token}`
        },
        json: true,
        body: newPhone
      });
      response.phone.should.eql(newPhone.phone);
    });
  });

  context('Create and update item', function () {
    it('create item with user_id field check', function* () {
      let response = yield request({
        method: 'post',
        url: getURL('/item'),
        headers: {
          'Authorization': `Bearer ${token}`
        },
        json: true,
        body: newItemData
      });
      itemId = response.id;
      response.title.should.eql(newItemData.title);
      response.User.email.should.eql(newUserData.email);
    });

    it('update item with user_id field check', function* () {
      let response = yield request({
        method: 'put',
        url: getURL(`/item/${itemId}`),
        headers: {
          'Authorization': `Bearer ${token}`
        },
        json: true,
        body: { price: '5000' }
      });
      response.price.should.eql(5000);
      response.User.email.should.eql(newUserData.email);
    });


  })
})
